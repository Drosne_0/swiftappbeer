//
//  MasterViewController.swift
//  PunkApp
//
//  Created by Denys DROSNE on 22/01/2019.
//  Copyright © 2019 Denys DROSNE. All rights reserved.
//
import UIKit

class Beer : Codable {
    init(id: Int, name: String, description: String, tags: String, alc: Float, imgURL: String, first_brewed: String) {
        self.id = id
        self.name = name
        self.description = description
        self.tagline = tags
        self.abv = alc
        self.image_url = imgURL
        self.first_brewed = first_brewed
    }
    let id: Int
    let name: String
    let description: String
    let tagline: String
    let abv: Float
    let image_url: String
    let first_brewed: String
}

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var objects = [Any]()
    

    func request(urlString: String, completionHandler: @escaping (Data?) -> Void) {
        guard let url = URL(string: urlString) else {
            completionHandler(nil)
            return
        }
        
        //Background thread
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url)
            
            // Main/UI thread
            DispatchQueue.main.async {
                completionHandler(data)
            }
        }
        
    }
    //========================
    func jsonParse(data: Data?) {
        if let d = data {
            let jsonDecoder = JSONDecoder()
            do{
                let beerArray = try jsonDecoder.decode([Beer].self, from: d)
            
                for beer in beerArray {
                    insertNewObject(self, beer: beer)
                    
                }
            }
            catch let error {
                print("Error: \(error)") }
            
        }
        
    }
        
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.tableView.addSubview(self.freshControl)
        request(urlString: "https://api.punkapi.com/v2/beers", completionHandler: jsonParse)
        
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    func insertNewObject(_ sender: Any, beer: Beer) {
        objects.insert(beer, at: 0)
        let indexPath = IndexPath(row: 0, section: 0)
        tableView.insertRows(at: [indexPath], with: .automatic)
    }
    
    lazy var freshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.black
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        request(urlString: "https://api.punkapi.com/v2/beers", completionHandler: jsonParse)
        self.tableView.reloadData()
        refreshControl.endRefreshing()
    }
    
    

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let object = objects[indexPath.row] as! Beer
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let object = objects[indexPath.row] as! Beer
        cell.textLabel!.text = object.name
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            objects.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }


}

