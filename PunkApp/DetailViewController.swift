//
//  DetailViewController.swift
//  PunkApp
//
//  Created by Denys DROSNE on 22/01/2019.
//  Copyright © 2019 Denys DROSNE. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var TitleBeer: UINavigationItem!
    @IBOutlet weak var imageScreen: UIImageView!
    @IBOutlet weak var DescriptionText: UITextView!
    @IBOutlet weak var degLabel: UILabel!
    @IBOutlet weak var tagsView: UITextView!
    @IBOutlet weak var firstBrewedLabel: UILabel!
    


    
    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem, isViewLoaded {
            TitleBeer.title = detail.name
            DescriptionText.text = detail.description
            degLabel.text = detail.abv.description + " °"
            tagsView.text = "Tags : " + detail.tagline
            firstBrewedLabel.text = detail.first_brewed
            let url = URL(string: detail.image_url)
            
            let task = URLSession.shared.dataTask(with: url!) { data, response, error in
                guard let data = data, error == nil else { return }
                
                DispatchQueue.main.async() {    // execute on main thread
                    self.imageScreen.image = UIImage(data: data)
                }
            }
            
            task.resume()
            
            
        }
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view, typically from a nib.
        configureView()
    }

    var detailItem: Beer? {
        didSet {
            // Update the view.
            configureView()
        }
    }


}

